var Engine = function( canvasId, gameOfLifeObj ) {

    this.gameOfLife = gameOfLifeObj;
    
    this.canvasId = canvasId;
    this.canvas = document.getElementById( this.canvasId );
    this.canvas.width = this.gameOfLife.worldCols;
    this.canvas.height = this.gameOfLife.worldLines;
    this.context = this.canvas.getContext( '2d' );

}

Engine.prototype.next = function() {

    this.gameOfLife.next();
    this.draw();

}

Engine.prototype.draw = function() {

    this.context.clearRect ( 0 , 0 , this.canvas.width, this.canvas.height );
    for( var i = 0; i < this.canvas.height; i++) {    
        for( var j = 0; j < this.canvas.width; j++) {
            if( this.gameOfLife.worldGrid[ i ][ j ] == 1 )
                this.context.fillRect( j, i, 1, 1 );
        }    
    }

}
