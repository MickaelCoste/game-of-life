var GameOfLife = function( lines, cols ) {

    this.worldLines = lines;
    this.worldCols = cols;
    
    this.worldGrid = this.generateEmptyWorldGrid();
    this.worldNeighboursCountGrid = this.generateEmptyWorldGrid();
    
    this.tickCount = 0;

}

GameOfLife.prototype.next = function() {

    this.updateNeighboursCountGrid();
    this.updateWorldGrid();
    
    this.tickCount++;

}

GameOfLife.prototype.updateNeighboursCountGrid = function() {

    for( var i = 0; i < this.worldLines; i++ ) {        
        for( var j = 0; j < this.worldCols; j++ ) {        
            neighboursCount = 0;        
            neighbours = this.getCaseNeighbours( i, j );            
            for( var k = 0; k < neighbours.length; k++)            
                neighboursCount += this.worldGrid[ neighbours[k][0] ][ neighbours[k][1] ];
            this.worldNeighboursCountGrid[i][j] = neighboursCount;            
        }        
    }

}

GameOfLife.prototype.updateWorldGrid = function() {

    for( var i = 0; i < this.worldLines; i++ ) {
        for( var j = 0; j < this.worldCols; j++ ) {
            if( this.worldNeighboursCountGrid[i][j] < 2 || this.worldNeighboursCountGrid[i][j] > 3 ) { this.worldGrid[i][j] = 0; }
            else if( this.worldNeighboursCountGrid[i][j] == 3 ) { this.worldGrid[i][j] = 1; }
        }
    }

}

GameOfLife.prototype.generateEmptyWorldGrid = function() {
    
    var worldGrid = [];
    for( var i = 0; i < this.worldLines; i++ ) {
        worldGrid[i] = [];
        for( var j = 0; j < this.worldCols; j++ )
            worldGrid[i][j] = 0;
    }
    return worldGrid;

}

GameOfLife.prototype.activeCase = function( line, col ) {

    this.worldGrid[line][col] = 1;

}

GameOfLife.prototype.getCaseNeighbours = function( line, col ) {

    var neighboursCoordinates = [];
    for( var i = line - 1; i <= line + 1; i++ ) {
        if( i < 0 ) { neighbourLine = this.worldLines - 1; }
        else if ( i > this.worldLines - 1 ) { neighbourLine = 0; }
        else { neighbourLine = i; }
        
        for( var j = col - 1; j <= col + 1; j++ ) {
            if( i == line && j == col ) { continue; }
            if( j < 0 ) { neighbourCol = this.worldCols - 1; }
            else if ( j > this.worldCols - 1 ) { neighbourCol = 0; }
            else { neighbourCol = j; }
            neighboursCoordinates[ neighboursCoordinates.length ] = [ neighbourLine, neighbourCol ];
        }
    }
    return neighboursCoordinates;

}
